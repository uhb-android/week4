package id.ac.uhb.week4.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import id.ac.uhb.week4.R;
import id.ac.uhb.week4.databinding.FragmentGuestDetailBinding;
import id.ac.uhb.week4.model.Guest;
import id.ac.uhb.week4.model.Guest_Table;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GuestDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GuestDetailFragment extends Fragment {


    private static final String ARG_PARAM1 = "guest_id";
    private long mGuestId;
    FragmentGuestDetailBinding binding;
    Guest selectedGuest;
    public GuestDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param guestId Parameter 1.
     * @return A new instance of fragment GuestDetailFragment.
     */

    public static GuestDetailFragment newInstance(long guestId) {
        GuestDetailFragment fragment = new GuestDetailFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, guestId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGuestId = getArguments().getLong(ARG_PARAM1);

        }
        selectedGuest = SQLite.select().from(Guest.class)
                .where(Guest_Table.id.eq((int)mGuestId)).querySingle();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentGuestDetailBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(selectedGuest!=null){
            binding.detailName.setText(String.format("Nama : %s",selectedGuest.getName()));
            binding.detailPhone.setText(String.format("Phone : %s",selectedGuest.getPhone()));
            binding.detailAddress.setText(String.format("Address : %s",selectedGuest.getAddress()));
            binding.detailEmail.setText(String.format("E-mail : %s",selectedGuest.getEmail()));
        }
    }
}