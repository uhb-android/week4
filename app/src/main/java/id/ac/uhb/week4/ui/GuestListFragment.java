package id.ac.uhb.week4.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import id.ac.uhb.week4.R;
import id.ac.uhb.week4.databinding.FragmentGuestListBinding;
import id.ac.uhb.week4.model.Guest;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GuestListFragment} factory method to
 * create an instance of this fragment.
 */
public class GuestListFragment extends Fragment {

    FragmentGuestListBinding binding;
    List<Guest> mItems;
    GuestListAdapter mAdapter;
    public GuestListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItems= SQLite.select().from(Guest.class).queryList();
        mAdapter = new GuestListAdapter(getActivity(),R.layout.guest_list_item,mItems);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentGuestListBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter.notifyDataSetChanged();
        binding.listGuest.setAdapter(mAdapter);
        binding.listGuest.invalidate();
        binding.listGuest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("List ","guest id : "+Long.toString(id));
                showGuestDetail(id);
            }
        });
        binding.listGuest.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void showGuestDetail(long id) {
//        GuestDetailFragment detailFragment = GuestDetailFragment.newInstance(id);

        Bundle bundle = new Bundle();
        bundle.putLong("guest_id",id);
        NavHostFragment.findNavController(GuestListFragment.this)
                .navigate(R.id.guestToDetail,bundle);
    }
}