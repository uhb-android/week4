package id.ac.uhb.week4.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import id.ac.uhb.week4.databinding.GuestListItemBinding;
import id.ac.uhb.week4.model.Guest;

public class GuestListAdapter extends ArrayAdapter {
    Context mContext;
    int Layout;
    List<Guest> mItems;
    GuestListItemBinding binding;
    public GuestListAdapter(@NonNull Context context, int resource, @NonNull List<Guest> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.Layout = resource;//resource layout
        this.mItems = objects;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).getId();
    }

    @Nullable
    @Override
    public Guest getItem(int position) {
        return mItems.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        binding = GuestListItemBinding.inflate(inflater);
        Guest item = mItems.get(position);
        if(item!=null){
            binding.guestItemName.setText(item.getName());
            binding.guestItemPhone.setText(item.getPhone());
        }

        return binding.getRoot();
    }
}
