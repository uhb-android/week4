package id.ac.uhb.week4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import id.ac.uhb.week4.databinding.ActivityMainBinding;
import id.ac.uhb.week4.model.Guest;
import id.ac.uhb.week4.model.Guest_Table;
import id.ac.uhb.week4.ui.HomeFragment;

public class MainActivity extends AppCompatActivity {
    private static final int MY_REQUEST_CODE = 212;
//    HomeFragment homeFragment;
    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager=getSupportFragmentManager();
        setContentView(R.layout.activity_main);
        checkPermission();
//        homeFragment = new HomeFragment();
//        fragmentManager.beginTransaction()
//                .replace(R.id.container_main,homeFragment)
//                .commit();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void checkDatabaseTamu() {
        List<Guest> daftarTamus = SQLite.select().from(Guest.class).queryList();
        for(Guest tamu:daftarTamus){
            Log.d("Guest","tamu : "+tamu.toString());
        }
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_REQUEST_CODE);

                return;
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==MY_REQUEST_CODE && grantResults[0]==PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "Permission menulis di storage sudah diijinkan", Toast.LENGTH_SHORT).show();
        }
    }
}