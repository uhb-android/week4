package id.ac.uhb.week4.ui;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import id.ac.uhb.week4.R;
import id.ac.uhb.week4.databinding.FragmentRegisterBinding;
import id.ac.uhb.week4.model.Guest;
import id.ac.uhb.week4.model.Guest_Table;
import id.ac.uhb.week4.utils.AppUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {

    FragmentRegisterBinding binding;
    String nama;
    String phone;
    String address;
    String email;
    Typeface mFont;
    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFont = AppUtils.getFont(getContext(),"OpenSans-Bold");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_register, container, false);
        binding = FragmentRegisterBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetForm();
            }
        });
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveForm();
            }
        });
        binding.judulProgram.setTypeface(mFont);
    }
    private void saveForm() {
        nama = binding.editName.getText().toString();
        phone = binding.editPhone.getText().toString();
        address = binding.editAddress.getText().toString();
        email = binding.editEmail.getText().toString();
        if(nama.isEmpty()){
            Toast.makeText(getActivity(), "Nama tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }
        if(phone.isEmpty()){
            Toast.makeText(getActivity(), "Nomor Ponsel Harap diisi!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(email.isEmpty()){
            Toast.makeText(getActivity(), "Alamat E-mail harap di isi juga!", Toast.LENGTH_SHORT).show();
            return;
        }
        simpanData();

    }

    private void simpanData() {
        Guest tamux = SQLite.select().from(Guest.class)
                .where(Guest_Table.email
                        .eq(email))
                .querySingle();
        if(tamux!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("OOPS!");
            builder.setMessage("Alamat E-mail sudah Ada bro!");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
            return;
        }
        Guest tamu = new Guest();
        tamu.setName(nama);
        tamu.setPhone(phone);
        tamu.setAddress(address);
        tamu.setEmail(email);
        tamu.save();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Terima Kasih!");
        builder.setMessage("Informasi Anda sudah tersimpan di database Kami!");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void resetForm() {
        binding.editName.setText("");
        binding.editPhone.setText("");
        binding.editAddress.setText("");
        binding.editEmail.setText("");
    }
}