package id.ac.uhb.week4.model;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
    public static final String NAME="guest_db";
    public static final int VERSION = 1;
}
